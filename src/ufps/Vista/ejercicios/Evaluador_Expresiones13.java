/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista.ejercicios;

import java.util.Scanner;
import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author madar
 */
public class Evaluador_Expresiones13 {
    
    public static void main(String[] args) {
        
        String cadena;
        for(int i=0; i<100;i++){
        System.out.println("Digite una cadena con () o [] o {} ");
        Scanner teclado = new Scanner(System.in);
        cadena=teclado.nextLine();
        System.out.println("Evaluando su expresion : "+evaluador(cadena)+"\n");
        }
        
    }
    
    
    private static boolean evaluador(String cadena)
    { 
        Pila<String> p=new Pila();
       boolean x=false;
        for (int i = 0; i < cadena.length(); i++){
        char letra = cadena.charAt(i);
            if(letra=='('){
             p.push("(");
             x=true;
             } 
            if(letra=='{'){
             p.push("{");
             x=true;
             }
            if(letra=='['){
             p.push("[");
             x=true;
             }
             if(letra==')'&&!p.esVacia()){
                 if(p.getTope().equals("(")){
                  p.pop();}
                 else {
                   i=cadena.length();
                   x=false;
                 }
             }
             
             if(letra=='}'&&!p.esVacia()){
                 if(p.getTope().equals("{")){
                  p.pop();}
                 else {
                   i=cadena.length();
                   x=false;
                 }
             }
             if(letra==']'&&!p.esVacia()){
                 if(p.getTope().equals("[")){
                  p.pop();}
                 else {
                   i=cadena.length();
                   x=false;
                 }
             }
         }
        if(!p.esVacia())x=false;
        
        
        // :)
        
        return x;
        
    }
}
