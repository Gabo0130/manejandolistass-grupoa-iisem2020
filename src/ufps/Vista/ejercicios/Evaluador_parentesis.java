/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista.ejercicios;

import java.util.Scanner;
import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author madar
 */
public class Evaluador_parentesis {
    
    public static void main(String[] args) {
        
        String cadena;
        System.out.println("Digite una cadena con ()");
        Scanner teclado = new Scanner(System.in);
        cadena=teclado.nextLine();
        System.out.println("Evaluando sus paréntesis : "+evaluador(cadena));
        
    }
    
    
    private static boolean evaluador(String cadena)
    { 
        Pila<String> p=new Pila();
        int aux = 0;
        boolean x=false;
        for (int i = 0; i < cadena.length(); i++){
        char letra = cadena.charAt(i);
            if(letra=='('){
             p.push("(");
             aux++;
             x=true;
             }else if(letra==')'&&aux>0){
             p.pop();
             aux--;
             }else {
             i=cadena.length();
             x=false;
             }
         }
    
        // :)
        
        return x;
        
    }
}
