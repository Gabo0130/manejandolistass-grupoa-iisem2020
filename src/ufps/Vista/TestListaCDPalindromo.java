/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class TestListaCDPalindromo {
    
    public static void main(String[] args) {
        ListaCD<Integer> enteros = new ListaCD<Integer>();
        enteros.insertarFin(1);
        enteros.insertarFin(2);
        enteros.insertarFin(3);
        enteros.insertarFin(4);
        enteros.insertarFin(4);
        enteros.insertarFin(3);
        enteros.insertarFin(2);
        enteros.insertarFin(1);
        ListaCD<String> letras= new ListaCD<String>();
        letras.insertarFin("a");
        letras.insertarFin("n");
        letras.insertarFin("n");
        letras.insertarFin("a");
        ListaCD<Persona> personas=new ListaCD<Persona>();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
        personas.insertarInicio(new Persona(4,"diana"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(1,"madarme"));
        System.out.println(enteros.toString());
        System.out.println("palindromo enteros "+enteros.palindromo());
        System.out.println(personas.toString());
        System.out.println("palindromo persona "+personas.palindromo());
        System.out.println(letras.toString());
        System.out.println("palindromo letras "+letras.palindromo());
    }
    
}
