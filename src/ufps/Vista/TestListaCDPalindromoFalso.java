/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class TestListaCDPalindromoFalso {
    
    public static void main(String[] args) {
        ListaCD<Integer> enteros = new ListaCD<Integer>();
        enteros.insertarFin(1);
        enteros.insertarFin(2);
        enteros.insertarFin(3);
        enteros.insertarFin(4);
        enteros.insertarFin(5);
        enteros.insertarFin(6);
        enteros.insertarFin(7);
        enteros.insertarFin(8);
        ListaCD<String> letras= new ListaCD<String>();
        letras.insertarFin("a");
        letras.insertarFin("n");
        letras.insertarFin("n");
        letras.insertarFin("e");
        ListaCD<Persona> personas=new ListaCD<Persona>();
        personas.insertarFin(new Persona(1,"madarme"));
        personas.insertarFin(new Persona(3,"gederson"));
        personas.insertarFin(new Persona(4,"diana"));
        personas.insertarFin(new Persona(5,"javier"));
        personas.insertarFin(new Persona(6,"jhony"));
        personas.insertarFin(new Persona(7,"jose"));
        System.out.println(enteros.toString());
        System.out.println("palindromo enteros "+enteros.palindromo());
        System.out.println(personas.toString());
        System.out.println("palindromo persona "+personas.palindromo());
        System.out.println(letras.toString());
        System.out.println("palindromo letras "+letras.palindromo());
    }
    
}
