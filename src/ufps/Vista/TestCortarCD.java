/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author hp
 */
public class TestCortarCD {
    public static void main(String[] args) {
        
        ListaCD<Integer> lista = new ListaCD<Integer>();
        lista.insertarFin(10);
        lista.insertarFin(100);
        lista.insertarFin(1000);
        lista.insertarFin(10000);
        lista.insertarFin(100000);
        lista.insertarFin(1000000);
         System.out.println("Lista sin cortar "+lista.toString());
         System.out.println("Cortando lista... "+lista.cortar(0, 2).toString());
         System.out.println("Lista cortada "+lista.toString()+" Nuevo tamaño "+lista.getTamano());
       
    }
}