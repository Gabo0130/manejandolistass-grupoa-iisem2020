/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class TestListaCD {
    
    public static void main(String[] args) {
        ListaCD<Persona> personas=new ListaCD();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
        System.out.println(personas.getTamano());
        
        
        ListaCD<Persona> personas2=new ListaCD();
        personas2.insertarFin(new Persona(1,"madarme"));
        personas2.insertarFin(new Persona(3,"gederson"));
        personas2.insertarFin(new Persona(4,"diana"));
        System.out.println(personas2);
    }
}
