/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;
import ufps.Modelo.Persona;

/**
 *
 * @author madarme
 */
public class ListaCD<T> {
    
    private NodoD<T> cabecera;
    private int tamano=0;

    public ListaCD() {
        this.cabecera=new NodoD();
        this.cabecera.setInfo(null);
        this.cabecera.setSiguiente(this.cabecera);
        this.cabecera.setAnterior(cabecera);
         
    }

    public int getTamano() {
        return tamano;
    }
    
    
    public void insertarInicio(T info)
    {
    NodoD<T> nuevo=new NodoD();
    nuevo.setInfo(info);
    nuevo.setSiguiente(this.cabecera.getSiguiente());
    //El anterior de nuevo nodo ES la cabecera
    nuevo.setAnterior(this.cabecera);
    //El siguiente de cabecera es el nuevo nodo
    this.cabecera.setSiguiente(nuevo);
    //El siguiente del nuevo nodo SU anterior ES el nuevo nodo
    nuevo.getSiguiente().setAnterior(nuevo);
    //Aumentar la cardinalidad
    this.tamano++;
    }
    
    
    public void insertarFin(T info)
    {
    NodoD<T> nuevo=new NodoD();
    nuevo.setInfo(info);
    //El anterior de nuevo es el anterior de cabecera
    nuevo.setAnterior(this.cabecera.getAnterior());
    //El siguiente de nuevo es cabecera
    nuevo.setSiguiente(this.cabecera);
    //El anterior de cabecera su siguiente ES nuevo nodo
    this.cabecera.getAnterior().setSiguiente(nuevo);
    //El anterior de cabecera es ahora nuevo
    this.cabecera.setAnterior(nuevo);
    //Aumento cardinalidad
    this.tamano++;
    }
    
    public boolean esVacia()
    {
    // Método 1: tamano ==0 
    // Método 2:
    return this.cabecera==this.cabecera.getSiguiente() && this.cabecera==this.cabecera.getAnterior();
    }

    @Override
    public String toString() {
        String msg="ListaCD{";
        
        for (NodoD<T> x=this.cabecera.getSiguiente();x!=this.cabecera;x=x.getSiguiente())
            msg+=x.getInfo().toString()+"<-->";
        
        return msg+"}";
    }
    
    public T eliminar(int pos)
    {
        try {
            /**
             * Verificar que pos sea válida pos< cardinalidad y no es vacía y pos>=0 xxx
             * Nodo actual lo colocamos en getPos(pos) xxx
             * Nodo anterior estará nodoactual su anterior  xxx
             * El siguiente de nodoAnt  será el siguiente de nodo actual xxx
             * El siguiente de nodoactual su anterior será el nodo anterior 
             * Desunir el nodoactual
             * Disminuir cardinalidad
             * Retorno el info del nodoactual
             */
            NodoD<T> nodoActual=this.getPos(pos);
            NodoD<T> nodoAnt=nodoActual.getAnterior();
            nodoAnt.setSiguiente(nodoActual.getSiguiente());
            nodoActual.getSiguiente().setAnterior(nodoAnt);
            this.desUnir(nodoActual);
            this.tamano--;
            return nodoActual.getInfo();
            
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    
    }
  
    
    
    private void desUnir(NodoD<T> x)
    {
        x.setSiguiente(x);
        x.setAnterior(x);
    }
    
    
    private NodoD<T> getPos(int pos) throws Exception
    {
        //  Verificar que pos sea válida pos< cardinalidad y no es vacía y pos>=0
        //Es vacio sobra...
        if(this.esVacia() || pos<0 || pos>=this.tamano)
            throw new Exception("La posición "+pos+" No es válida en la lista");
      
      
        //Contribución de Carlos Eduardo Contreras Mendoza
      if(pos==this.tamano-1)  
          return this.cabecera.getAnterior();
          
          
      NodoD<T> nodoPos=this.cabecera.getSiguiente();
      while(pos-->0)
          nodoPos=nodoPos.getSiguiente();
      
      return nodoPos;
            
    }
    
    
    public T get(int pos)
    {
    
        try {
            return this.getPos(pos).getInfo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    
    public void set(int pos, T infoNuevo)
    {
    
        
        try {
                this.getPos(pos).setInfo(infoNuevo);
        } catch (Exception ex) {
                System.out.println(ex.getMessage());
            
        }
    }
    
    
    /*
        Corta los nodos de posición inical a pos final y pasa estos nodos a la lista l2
    
    Condiciones:
    1. posInicial<=posfinal
    2. NO SE DEBEN CREAR NODOS, SOLO SE PUEDEN REFERENCIAR ( NO USAR INSERFIN O INSRINICIO)
    
    Ejemplo: l1=<12,16,17,11,13,10> , l2=l1.cortar(2,4) 
             l1=<12,16,10> y l2=<17,11,13>
    */
    public ListaCD<T> cortar(int posInicial, int posFinal)
    {
    
        ListaCD<T> l2=new ListaCD<T>();
        if(posInicial<=posFinal){
            try {
                NodoD<T> nodoInicial=this.getPos(posInicial);
                NodoD<T> nodoFinal=this.getPos(posFinal);
                nodoInicial.getAnterior().setSiguiente(nodoFinal.getSiguiente());
                nodoFinal.getSiguiente().setAnterior(nodoInicial.getAnterior());
                l2.cabecera.setSiguiente(nodoInicial);
                nodoInicial.setAnterior(l2.cabecera);
                nodoFinal.setSiguiente(l2.cabecera);
                l2.cabecera.setAnterior(nodoFinal);
                int cambio=(posFinal+2)-(posInicial+1);
                this.tamano=this.tamano-cambio;
                l2.tamano=cambio;
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                           }
        }
            
        
        return l2;
    }   
    
    
public boolean palindromo(){
        boolean palindromo=false;
        NodoD<T> nodoFinal=this.cabecera.getAnterior();
        
        for(NodoD<T> inicial=this.cabecera.getSiguiente(); inicial!=this.cabecera; inicial=inicial.getSiguiente()){
            if(inicial.getInfo().equals(nodoFinal.getInfo())){
                palindromo=true;
            }else{
             palindromo=false;
             inicial=this.cabecera;
            }
            nodoFinal=nodoFinal.getAnterior();
        }
        return palindromo;
    }
  public void pasarAlFin(T info){
       
  for (NodoD<T> x=this.cabecera.getSiguiente();x!=this.cabecera;x=x.getSiguiente()){
    if(false){
            // no se como saber si es mayor o menor ;/ 
            x.setAnterior(x.getSiguiente());
            x.setSiguiente(x.getAnterior());
            this.cabecera.getAnterior().setSiguiente(x);
            this.cabecera.setAnterior(x);
            
        }
  
  
  }
}
}
